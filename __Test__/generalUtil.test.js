import { isPrime, fibonacci, renamePokemon } from '../Utils/generalUtil';

describe('isPrime', () => {
  it('should return false for numbers less than or equal to 1', () => {
    expect(isPrime(0)).toBe(false);
    expect(isPrime(1)).toBe(false);
    expect(isPrime(-7)).toBe(false);
  });

  it('should return true for prime numbers', () => {
    expect(isPrime(2)).toBe(true);
    expect(isPrime(3)).toBe(true);
    expect(isPrime(17)).toBe(true);
  });

  it('should return false for non-prime numbers', () => {
    expect(isPrime(4)).toBe(false);
    expect(isPrime(6)).toBe(false);
    expect(isPrime(15)).toBe(false);
  });
});

describe('fibonacci', () => {
  it('should return 0 for index 0', () => {
    expect(fibonacci(0)).toBe(0);
  });

  it('should return 1 for index 1', () => {
    expect(fibonacci(1)).toBe(1);
  });

  it('should calculate Fibonacci sequence correctly', () => {
    expect(fibonacci(2)).toBe(1);
    expect(fibonacci(3)).toBe(2);
    expect(fibonacci(4)).toBe(3);
    expect(fibonacci(5)).toBe(5);
    expect(fibonacci(6)).toBe(8);
    // Add more test cases as needed
  });
});

describe('renamePokemon', () => {
  it('should rename Pokemon correctly', () => {
    expect(renamePokemon('bulbasaur', 1)).toBe('bulbasaur-1');
    expect(renamePokemon('charmander', 3)).toBe('charmander-2');
    expect(renamePokemon('pikachu', 2)).toBe('pikachu-1');
    // Add more test cases as needed
  });
});
