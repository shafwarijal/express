import axios from 'axios';
import pokemonController from '../Contoller/pokemonController'; // Path to your controller
import { getData, setData, generateId } from '../Helper/dbHelper';
import responseHelper from '../Helper/responseHelper';
import { isPrime, renamePokemon } from '../Utils/generalUtil';

jest.mock('axios');
jest.mock('../Helper/dbHelper');
jest.mock('../Helper/responseHelper');
jest.mock('../Utils/generalUtil');

describe('pokemonController', () => {
  let mockReq;
  let mockRes;
  let mockNext;

  beforeEach(() => {
    mockReq = {
      params: {},
      body: {},
    };
    mockRes = {
      status: jest.fn(() => mockRes),
      json: jest.fn(),
    };
    mockNext = jest.fn();
    jest.clearAllMocks();
    // mockNext = jest.fn();
  });

  describe('getPokemons', () => {
    it('should get a list of pokemons', async () => {
      const mockData = {
        data: {
          results: [{ name: 'pikachu' }, { name: 'charmander' }],
        },
      };

      axios.get.mockResolvedValue(mockData);

      await pokemonController.getPokemon(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon?limit=100&offset=50');
      expect(responseHelper).toHaveBeenCalledWith(
        mockRes,
        200,
        ['pikachu', 'charmander'],
        'Succesfully get pokemon list'
      );
    });

    it('should handle error when getting pokemons', async () => {
      axios.get.mockRejectedValueOnce(new Error('Network Error'));

      await pokemonController.getPokemon(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon?limit=100&offset=50');
      expect(responseHelper).toHaveBeenCalledWith(mockRes, 500);
    });
  });

  describe('getPokemonDetail', () => {
    it('should get pokemon detail', async () => {
      const mockData = {
        data: {
          // Mocked data from the API response
          name: 'meowth',
          height: 40,
          weight: 60,
        },
      };

      axios.get.mockResolvedValue(mockData);
      mockReq.params.name = 'meowth';

      await pokemonController.getPokemonDetail(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon/meowth');
      expect(responseHelper).toHaveBeenCalledWith(
        mockRes,
        200,
        {
          // Mocked data from the API response
          name: 'meowth',
          height: 40,
          weight: 60,
        },
        'Succesfully get pokemon detail'
      );
    });

    it('should handle not found and return 400', async () => {
      const mockError = { response: { status: 404 } };
      axios.get.mockRejectedValue(mockError);
      mockReq.params.name = 'Missing';

      await pokemonController.getPokemonDetail(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon/Missing');
      expect(responseHelper).toHaveBeenCalledWith(mockRes, 400, null, `Invalid name`);
    });

    it('should handle error when getting pokemon detail', async () => {
      axios.get.mockRejectedValueOnce(new Error('Network Error'));
      mockReq.params.name = 'meowth';

      await pokemonController.getPokemonDetail(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon/meowth');
      expect(responseHelper).toHaveBeenCalledWith(mockRes, 500);
    });
  });

  // describe('createMyPokemon', () => {});

  describe('createMyPokemon', () => {
    it('should create a new Pokemon successfully', async () => {
      const mockData = {
        pokemons: [
          { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '3', release: 0 },
          { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
        ],
      };

      mockReq = {
        body: { name: 'pikachu', nameEvolusi: 'pikachu', height: '10' },
      };

      getData.mockReturnValue(mockData);
      generateId.mockReturnValue(3); // Mocking the ID generation
      Math.random = jest.fn().mockReturnValue(0.7);

      pokemonController.createMyPokemon(mockReq, mockRes);

      // Math.random = jest.fn().mockReturnValue(0.7);

      const expectedMockData = {
        pokemons: [
          { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '3', release: 0 },
          { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
          { id: 3, name: 'pikachu', nameEvolusi: 'pikachu', height: '10', release: 0 },
        ],
      };

      expect(setData).toHaveBeenCalledWith(expectedMockData);
      expect(responseHelper).toHaveBeenCalledWith(
        mockRes,
        200,
        { id: 3, name: 'pikachu', nameEvolusi: 'pikachu', height: '10', release: 0 },
        'Pokemon added successfully'
      );
    });

    it('should handle adding an existing Pokemon', async () => {
      const mockData = {
        pokemons: [
          { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '3', release: 0 },
          { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
        ],
      };

      mockReq = {
        body: { name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '10' },
      };

      const nameBody = mockReq.body.name;
      getData.mockReturnValue(mockData);
      Math.random = jest.fn().mockReturnValue(0.7); // Mocking the random probability

      pokemonController.createMyPokemon(mockReq, mockRes);

      expect(responseHelper).toHaveBeenCalledWith(mockRes, 400, null, `${nameBody} already exists`);
    });

    it('should handle failure to add a new Pokemon', async () => {
      const mockData = {
        pokemons: [
          { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '3', release: 0 },
          { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
        ],
      };

      mockReq = {
        body: { name: 'pikachu', nameEvolusi: 'pikachu', height: '10' },
      };

      getData.mockReturnValue(mockData);
      generateId.mockReturnValue(3); // Mocking the ID generation
      Math.random = jest.fn().mockReturnValue(0.4);

      pokemonController.createMyPokemon(mockReq, mockRes);

      Math.random = jest.fn().mockReturnValue(0.7);

      expect(responseHelper).toHaveBeenCalledWith(
        mockRes,
        200,
        { id: 3, name: 'pikachu', nameEvolusi: 'pikachu', height: '10', release: 0 },
        'Failed to add Pokemon.'
      );
    });

    it('should handle internal server error', async () => {
      getData.mockImplementation(() => {
        throw new Error('Some error');
      });

      pokemonController.createMyPokemon(mockReq, mockRes);

      expect(responseHelper).toHaveBeenCalledWith(mockRes, 500);
    });
  });

  describe('getMyPokemon', () => {
    it('should get my Pokemon list successfully', async () => {
      const mockData = {
        pokemons: [
          { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '3', release: 0 },
          { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
        ],
      };
      getData.mockReturnValue(mockData);

      pokemonController.getMyPokemon(mockReq, mockRes);

      expect(getData).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(mockRes, 200, mockData.pokemons, 'Success to get pokemon');
    });

    it('should handle error and respond with 500', async () => {
      getData.mockImplementation(() => {
        throw new Error();
      });

      pokemonController.getMyPokemon(mockReq, mockRes);

      expect(responseHelper).toHaveBeenCalledWith(mockRes, 500);
    });
  });

  describe('getMyPokemonPrime', () => {
    it('should release a Pokemon successfully if random number is prime', async () => {
      const mockData = {
        pokemons: [
          { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '3', release: 0 },
          { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
        ],
      };
      getData.mockReturnValue(mockData);
      isPrime.mockReturnValue(true);

      pokemonController.getMyPokemonPrime(mockReq, mockRes);

      // Assert
      expect(getData).toHaveBeenCalled();
      expect(isPrime).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(mockRes, 200, mockData, 'Success to release pokemon');
    });

    it('should fail to release a Pokemon if random number is not prime', async () => {
      const mockData = {
        pokemons: [
          { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '3', release: 0 },
          { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
        ],
      };
      getData.mockReturnValue(mockData);
      isPrime.mockReturnValueOnce(false);

      pokemonController.getMyPokemonPrime(mockReq, mockRes);

      // Assert
      expect(getData).toHaveBeenCalled();
      expect(isPrime).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(mockRes, 200, null, `Failed to release pokemon`);
    });

    it('should handle errors by sending a 500 response', async () => {
      getData.mockImplementation(() => {
        throw new Error('Some error');
      });

      pokemonController.getMyPokemonPrime(mockReq, mockRes);

      expect(responseHelper).toHaveBeenCalledWith(mockRes, 500);
    });
  });

  describe('getMyPokemonPrimeNext', () => {
    const mockData = {
      pokemons: [
        { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '3', release: 0 },
        { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
      ],
    };
    it('should call next middleware if random number is prime and Pokemon name is found', async () => {
      getData.mockReturnValue(mockData);
      isPrime.mockReturnValue(true);

      mockReq = {
        params: { name: 'bulbasaur' },
      };

      pokemonController.getMyPokemonPrimeNext(mockReq, mockRes, mockNext);

      // Pengecekan
      expect(getData).toHaveBeenCalled();
      expect(isPrime).toHaveBeenCalled();
      expect(mockNext).toHaveBeenCalled();
    });

    it('should send a response with a message if random number is not prime and Pokemon name is found', async () => {
      getData.mockReturnValue(mockData);
      isPrime.mockReturnValueOnce(false);
      mockReq = {
        params: { name: 'bulbasaur' },
      };

      pokemonController.getMyPokemonPrimeNext(mockReq, mockRes, mockNext);

      // Pengecekan
      expect(getData).toHaveBeenCalled();
      expect(isPrime).toHaveBeenCalled();
      expect(mockNext).not.toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(mockRes, 200, null, 'Failed to release pokemon');
    });

    it('should send a response with a message if Pokemon name is not found', async () => {
      getData.mockReturnValue(mockData);
      mockReq = {
        params: { name: 'nonExistentName' },
      };

      const { name } = mockReq.params;
      pokemonController.getMyPokemonPrimeNext(mockReq, mockRes, mockNext);

      expect(getData).toHaveBeenCalled();
      expect(isPrime).not.toHaveBeenCalled();
      expect(mockNext).not.toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(mockRes, 400, null, `cant find pokemon with name: ${name}`);
    });

    it('should handle errors by sending a 500 response', async () => {
      getData.mockImplementation(() => {
        throw new Error('Some error');
      });

      pokemonController.getMyPokemonPrimeNext(mockReq, mockRes);

      expect(getData).toHaveBeenCalled();
      expect(mockNext).not.toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(mockRes, 500);
    });
  });

  describe('getMyPokemonRename', () => {
    const mockData = {
      pokemons: [
        { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '3', release: 0 },
        { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
      ],
    };

    it('should rename a Pokemon successfully', async () => {
      getData.mockReturnValue(mockData);

      mockReq = {
        params: { name: 'bulbasaur' },
      };
      renamePokemon.mockImplementation((nameEvolusi) => `${nameEvolusi}-0`);

      pokemonController.getMyPokemonRename(mockReq, mockRes);

      const expectedMockData = {
        pokemons: [
          { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur-0', height: '3', release: 1 },
          { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
        ],
      };

      // Check
      expect(getData).toHaveBeenCalled();
      expect(setData).toHaveBeenCalledWith(expectedMockData);
      expect(renamePokemon).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(
        mockRes,
        200,
        {
          id: 1,
          name: 'bulbasaur',
          nameEvolusi: 'bulbasaur-0',
          height: '3',
          release: 1,
        },
        `Success rename pokemon`
      );
    });

    it('should send a response with status 500 if an error occurs', async () => {
      getData.mockImplementation(() => {
        throw new Error('Some error');
      });

      await pokemonController.getMyPokemonRename(mockReq, mockRes);

      expect(getData).toHaveBeenCalled();
      expect(setData).not.toHaveBeenCalled();
      expect(renamePokemon).not.toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(mockRes, 500);
    });
  });
});
