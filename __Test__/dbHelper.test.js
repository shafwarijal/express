import { readFileSync, writeFileSync } from 'fs';
import { getData, setData, generateId } from '../Helper/dbHelper.js';

jest.mock('fs');

describe('dbHelper.js', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should get data from file', () => {
    const mockData = { pokemons: [{ id: 1, name: 'bulbasaur' }] };
    readFileSync.mockReturnValue(JSON.stringify(mockData));

    const result = getData();

    expect(result).toEqual(mockData);
    expect(readFileSync).toHaveBeenCalledWith(expect.anything());
  });

  it('should set data to file', () => {
    const mockData = { pokemons: [{ id: 1, name: 'bulbasaur' }] };
    setData(mockData);

    expect(writeFileSync).toHaveBeenCalledWith(expect.anything(), JSON.stringify(mockData, null, 2));
  });

  it('should generate new ID correctly', () => {
    const mockData = {
      pokemons: [
        { id: 1, name: 'bulbasaur' },
        { id: 2, name: 'charmander' },
      ],
    };
    readFileSync.mockReturnValue(JSON.stringify(mockData));

    const result = generateId();

    expect(result).toBe(3);
  });

  it('should start from ID 1 if no pokemons found', () => {
    const mockData = { pokemons: [] };
    readFileSync.mockReturnValue(JSON.stringify(mockData));

    const result = generateId();

    expect(result).toBe(1);
  });
});
