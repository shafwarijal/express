import responseHelper from '../Helper/responseHelper';

describe('responseHelper', () => {
  let mockRes;

  beforeEach(() => {
    mockRes = {
      status: jest.fn(() => mockRes),
      json: jest.fn(),
    };
  });

  it('should return internal server error response', () => {
    responseHelper(mockRes, 500);
    expect(mockRes.status).toHaveBeenCalledWith(500);
    expect(mockRes.json).toHaveBeenCalledWith({
      status: 'Error',
      statusCode: 500,
      message: 'Internal Server Error',
    });
  });

  it('should return not found response', () => {
    responseHelper(mockRes, 404);
    expect(mockRes.status).toHaveBeenCalledWith(404);
    expect(mockRes.json).toHaveBeenCalledWith({
      status: 'Error',
      statusCode: 404,
      message: 'Api Not Found',
    });
  });

  it('should return bad request response', () => {
    responseHelper(mockRes, 400, null, 'Invalid input');
    expect(mockRes.status).toHaveBeenCalledWith(400);
    expect(mockRes.json).toHaveBeenCalledWith({
      status: 'Error',
      statusCode: 400,
      message: 'Invalid input',
    });
  });

  it('should return success response with data', () => {
    const testData = { name: 'John', age: 30 };
    responseHelper(mockRes, 200, testData, 'Data fetched successfully');
    expect(mockRes.status).toHaveBeenCalledWith(200);
    expect(mockRes.json).toHaveBeenCalledWith({
      status: 'Success',
      statusCode: 200,
      data: testData,
      message: 'Data fetched successfully',
    });
  });

  it('should return success response without data', () => {
    responseHelper(mockRes, 200, null, 'No data found');
    expect(mockRes.status).toHaveBeenCalledWith(200);
    expect(mockRes.json).toHaveBeenCalledWith({
      status: 'Success',
      statusCode: 200,
      data: null,
      message: 'No data found',
    });
  });
});
