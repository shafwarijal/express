import axios from 'axios';
import responseHelper from '../Helper/responseHelper.js';
import { getData, setData, generateId } from '../Helper/dbHelper.js';
import { isPrime, renamePokemon } from '../Utils/generalUtil.js';

const pokemonController = {
  getPokemon: async (req, res) => {
    try {
      const pokemons = await axios.get('https://pokeapi.co/api/v2/pokemon?limit=100&offset=50');
      const data = pokemons?.data?.results?.map((item) => item.name);
      responseHelper(res, 200, data, 'Succesfully get pokemon list');
    } catch (error) {
      responseHelper(res, 500);
    }
  },

  getPokemonDetail: async (req, res) => {
    const namePokemon = req.params.name;
    try {
      const pokemon = await axios.get(`https://pokeapi.co/api/v2/pokemon/${namePokemon}`);
      const data = pokemon?.data;
      responseHelper(res, 200, data, 'Succesfully get pokemon detail');
    } catch (error) {
      const errStatus = error?.response?.status;
      if (errStatus === 404) {
        responseHelper(res, 400, null, `Invalid name`);
      }
      responseHelper(res, 500);
    }
  },

  createMyPokemon: async (req, res) => {
    try {
      const data = getData();
      const nameBody = req.body.name;
      const existingPokemon = data.pokemons.find((item) => item.name.toLowerCase() === nameBody.toLowerCase());

      if (existingPokemon) {
        responseHelper(res, 400, null, `${nameBody} already exists`);
        return;
      }

      const newPokemon = {
        id: generateId(),
        ...req.body,
        release: 0,
      };

      const probability = Math.random(); // Generates a random number between 0 and 1

      if (probability > 0.5) {
        setData({ pokemons: [...data.pokemons, newPokemon] });
        responseHelper(res, 200, newPokemon, 'Pokemon added successfully');
      } else {
        responseHelper(res, 200, newPokemon, 'Failed to add Pokemon.');
      }
    } catch (error) {
      responseHelper(res, 500);
    }
  },

  getMyPokemon: async (req, res) => {
    try {
      const data = getData();
      responseHelper(res, 200, data.pokemons, 'Success to get pokemon');
    } catch (error) {
      responseHelper(res, 500);
    }
  },

  getMyPokemonPrime: async (req, res) => {
    try {
      const data = getData();
      const randomNum = Math.floor(Math.random() * 100) + 1;

      if (isPrime(randomNum)) {
        responseHelper(res, 200, data, 'Success to release pokemon');
      } else {
        responseHelper(res, 200, null, `Failed to release pokemon`);
      }
      //   responseHelper(res, 200, data.pokemons);
    } catch (error) {
      responseHelper(res, 500);
    }
  },

  getMyPokemonPrimeNext: async (req, res, next) => {
    try {
      const { name } = req.params;
      const data = getData();

      const findName = data.pokemons.find((item) => item?.name?.toLowerCase() === name?.toLowerCase());

      if (findName) {
        const randomNum = Math.floor(Math.random() * 100) + 1;

        if (isPrime(randomNum)) {
          next();
        } else {
          responseHelper(res, 200, null, `Failed to release pokemon`);
        }
      } else {
        responseHelper(res, 400, null, `cant find pokemon with name: ${name}`);
      }
    } catch (error) {
      responseHelper(res, 500);
    }
  },

  getMyPokemonRename: async (req, res) => {
    try {
      const { name } = req.params;
      const data = getData();

      const newPokemon = data.pokemons.map((item) => {
        const selectedPokemon = item.name.toLowerCase() === name.toLowerCase();
        if (selectedPokemon) {
          item.nameEvolusi = renamePokemon(item.nameEvolusi, item.release);
          item.release += 1;
        }
        return item;
      });

      setData({ pokemons: newPokemon });

      const succesRename = newPokemon?.find((item) => item.name.toLowerCase() === name.toLowerCase());

      responseHelper(res, 200, succesRename, `Success rename pokemon`);
    } catch (error) {
      responseHelper(res, 500);
      // console.log(error);
    }
  },
};

export default pokemonController;
