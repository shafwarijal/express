export const isPrime = (num) => {
  if (num <= 1) {
    return false;
  }
  for (let i = 2; i <= Math.sqrt(num); i++) {
    if (num % i === 0) {
      return false;
    }
  }
  return true;
};

export const fibonacci = (index) => {
  if (index === 0) return 0;
  if (index === 1) return 1;

  let prev = 0;
  let current = 1;

  for (let i = 2; i <= index; i++) {
    const next = prev + current;
    prev = current;
    current = next;
  }

  return current;
};

export const renamePokemon = (nameEvolusi, release) => {
  const splitName = nameEvolusi.split('-');
  const originalName = splitName[0];
  const fiboKey = fibonacci(release);
  return `${originalName}-${fiboKey}`;
};
