import express from 'express';

// Import routes
import pokemonRouter from './Routes/pokemonRouter.js';

const app = express();
const port = 8080;
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.use('/api/v1/pokemon', pokemonRouter);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
