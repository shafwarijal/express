import { Router } from 'express';
import pokemonController from '../Contoller/pokemonController.js';

const router = Router();

router.get('/apipokemon', pokemonController.getPokemon);
router.get('/apipokemon/:name', pokemonController.getPokemonDetail);
router.post('/mypokemon', pokemonController.createMyPokemon);
router.get('/mypokemon', pokemonController.getMyPokemon);
router.get('/mypokemon/release', pokemonController.getMyPokemonPrime);
router.get('/mypokemon/rename/:name', pokemonController.getMyPokemonPrimeNext, pokemonController.getMyPokemonRename);

export default router;
