import { readFileSync, writeFileSync } from 'fs';

const dbPath = new URL('../Database/db.json', import.meta.url);

export const getData = () => JSON.parse(readFileSync(dbPath));
export const setData = (data) => writeFileSync(dbPath, JSON.stringify(data, null, 2));

export const generateId = () => {
  const data = JSON.parse(readFileSync(dbPath));
  const lastPokemon = data.pokemons[data.pokemons.length - 1];

  if (lastPokemon) {
    const nextId = lastPokemon.id + 1;
    return nextId;
  }
  return 1; // Start from ID 1 if no pokemons found
};
